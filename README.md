Hacker Appetite
====
http://haap.space

Rewriting the desktop version in Kotlin (using Ktor)

game wiki:  https://bitbucket.org/juliancanaves/haap/wiki/Home


KTOR
----

Ktor QuickStart
https://ktor.io/quickstart/index.html

Guides: How to create a plain website using ktor
https://ktor.io/quickstart/guides/website.html


Guides: How to implement a chat with WebSockets
https://ktor.io/quickstart/guides/chat.html
https://ktor.io/samples/app/chat.html


Guides: How to create an API using ktor
https://ktor.io/quickstart/guides/api.html


Network applicationframework
https://netty.io/



FIX: 
Logging
// The ApplicationEnvironment interface has a log property. You can access it inside an ApplicationCall with call.application.environment.log


TODO: 
ktor.deployment.watch patterns for automatic reload


Status pages to handle exceptions
https://ktor.io/servers/features/status-pages.html


EXTRA:

Configuring Ktor server
https://ktor.io/servers/configuration.html




GRADLE
-------

Gradle deployment
https://www.tutorialspoint.com/gradle/gradle_deployment.htm




Publish JAR artifact using Gradle to Artifactory
http://buransky.com/scala/publish-jar-artifact-using-gradle-to-artifactory/
(locally runing artifactory server)


! Upload artifact to Artifactory using Gradle
https://stackoverflow.com/questions/22352475/upload-artifact-to-artifactory-using-gradle


The Gradle Artifactory Plugin allows you to deploy your build artifacts and build information to Artifactory and also to resolve your build dependencies from Artifactory.
https://www.jfrog.com/confluence/display/RTF/Gradle+Artifactory+Plugin




The Gradle Wrapper
https://docs.gradle.org/current/userguide/gradle_wrapper.html


