package haap

import io.ktor.application.*
import io.ktor.features.*
import io.ktor.http.*
import io.ktor.response.*
import io.ktor.routing.*

fun Application.main() {
    install(DefaultHeaders)
    install(CallLogging)
    routing {
        root()
        bye()
        user()
        check()
        settings()
    }
/*
    install(StatusPages){
        exception<Throwable> {
            call.respond(HttpStatusCode.InternalServerError)
        }
    }
*/
}

// Extracted routes
fun Routing.root() {
    get("/") {
        call.respondText("Haap welcome", ContentType.Text.Html)
    }
}

fun Routing.bye() {
    get("/bye") {
        call.respondText("Haap bye!", ContentType.Text.Html)
    }
}

fun Routing.user() {
    get("/user/{login}") {
        val login = call.parameters["login"]
        call.respondText("Hello $login!", ContentType.Text.Html)
    }
}

fun Routing.check() {
    get("/check") {
        call.respondText("OK", ContentType.Text.Html)
    }
}

fun Routing.settings() {
    get("/settings") {
        val randomness: String = application.environment.config
            .propertyOrNull("haap.maps.randomness")?.getString()
            ?: "Nope"
        call.respondText("Map randomness is set to $randomness", ContentType.Text.Html)
    }
}


